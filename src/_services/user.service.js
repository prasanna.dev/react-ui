import config from 'config';
import { authHeader } from '../_helpers';

const auditData = [{
    "_id": "5f6221e8ecfbeb4554643d88",
    "username": "Sample2",
    "firstName": "Sample Name",
    "lastName": "Sample Last Name",
    "roleId": 2,
    "createdDate": "2020-09-16T14:32:08.680Z",
    "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
    "__v": 0,
    "loggedInDate": "2020-09-16",
    "loggedInUserIp": null,
    "id": "5f6221e8ecfbeb4554643d88"
  },{
    "username": "Sample091",
    "firstName": "Sample Name3",
    "lastName": "Sample Last Name3",
    "roleName": "AUDITOR",
    "loggedInDate": "2020-09-16",
    "loggedInUserIp":"localhost"
    
  },{
    "_id": "5f6221e8ecfbeb4554643d88",
    "username": "Sample3",
    "firstName": "Sample Name",
    "lastName": "Sample Last Name",
    "roleId": 2,
    "createdDate": "2020-09-16T14:32:08.680Z",
    "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
    "__v": 0,
    "loggedInDate": "2020-09-16",
    "loggedInUserIp": null,
    "id": "5f6221e8ecfbeb4554643d88"
  },{
    "username": "Sample34",
    "firstName": "Sample Name3",
    "lastName": "Sample Last Name3",
    "roleName": "AUDITOR",
    "loggedInDate": "2020-09-16",
    "loggedInUserIp":"localhost"
    
  },{
    "_id": "5f6221e8ecfbeb4554643d88",
    "username": "Sample4",
    "firstName": "Sample Name",
    "lastName": "Sample Last Name",
    "roleId": 2,
    "createdDate": "2020-09-16T14:32:08.680Z",
    "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
    "__v": 0,
    "loggedInDate": "2020-09-16",
    "loggedInUserIp": null,
    "id": "5f6221e8ecfbeb4554643d88"
  },{
    "username": "Sample37",
    "firstName": "Sample Name3",
    "lastName": "Sample Last Name3",
    "roleName": "AUDITOR",
    "loggedInDate": "2020-09-16",
    "loggedInUserIp":"localhost"
    
  },{
    "_id": "5f6221e8ecfbeb4554643d88",
    "username": "Sample5",
    "firstName": "Sample Name",
    "lastName": "Sample Last Name",
    "roleId": 2,
    "createdDate": "2020-09-16T14:32:08.680Z",
    "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
    "__v": 0,
    "loggedInDate": "2020-09-16",
    "loggedInUserIp": null,
    "id": "5f6221e8ecfbeb4554643d88"
  },{
    "username": "Sample10",
    "firstName": "Sample Name3",
    "lastName": "Sample Last Name3",
    "roleName": "AUDITOR",
    "loggedInDate": "2020-09-16",
    "loggedInUserIp":"localhost"
    
  },{
    "_id": "5f6221e8ecfbeb4554643d88",
    "username": "Sample6",
    "firstName": "Sample Name",
    "lastName": "Sample Last Name",
    "roleId": 2,
    "createdDate": "2020-09-16T14:32:08.680Z",
    "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
    "__v": 0,
    "loggedInDate": "2020-09-16",
    "loggedInUserIp": null,
    "id": "5f6221e8ecfbeb4554643d88"
  },{
    "username": "Sample073",
    "firstName": "Sample Name3",
    "lastName": "Sample Last Name3",
    "roleName": "AUDITOR",
    "loggedInDate": "2020-09-16",
    "loggedInUserIp":"localhost"
    
  },{
    "_id": "5f6221e8ecfbeb4554643d88",
    "username": "Sample092",
    "firstName": "Sample Name",
    "lastName": "Sample Last Name",
    "roleId": 2,
    "createdDate": "2020-09-16T14:32:08.680Z",
    "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
    "__v": 0,
    "loggedInDate": "2020-09-16",
    "loggedInUserIp": null,
    "id": "5f6221e8ecfbeb4554643d88"
  },{
    "username": "Sample13",
    "firstName": "Sample Name3",
    "lastName": "Sample Last Name3",
    "roleName": "AUDITOR",
    "loggedInDate": "2020-09-16",
    "loggedInUserIp":"localhost"
    
  },{
    "_id": "5f6221e8ecfbeb4554643d88",
    "username": "Sample8",
    "firstName": "Sample Name",
    "lastName": "Sample Last Name",
    "roleId": 2,
    "createdDate": "2020-09-16T14:32:08.680Z",
    "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
    "__v": 0,
    "loggedInDate": "2020-09-16",
    "loggedInUserIp": null,
    "id": "5f6221e8ecfbeb4554643d88"
  },{
    "username": "Sample14",
    "firstName": "Sample Name3",
    "lastName": "Sample Last Name3",
    "roleName": "AUDITOR",
    "loggedInDate": "2020-09-16",
    "loggedInUserIp":"localhost"
    
  },{
    "_id": "5f6221e8ecfbeb4554643d88",
    "username": "Sample376",
    "firstName": "Sample Name",
    "lastName": "Sample Last Name",
    "roleId": 2,
    "createdDate": "2020-09-16T14:32:08.680Z",
    "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
    "__v": 0,
    "loggedInDate": "2020-09-16",
    "loggedInUserIp": null,
    "id": "5f6221e8ecfbeb4554643d88"
  },{
    "username": "Sample16",
    "firstName": "Sample Name3",
    "lastName": "Sample Last Name3",
    "roleName": "AUDITOR",
    "loggedInDate": "2020-09-16",
    "loggedInUserIp":"localhost"
    
  },{
    "_id": "5f6221e8ecfbeb4554643d88",
    "username": "Sample043",
    "firstName": "Sample Name",
    "lastName": "Sample Last Name",
    "roleId": 2,
    "createdDate": "2020-09-16T14:32:08.680Z",
    "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
    "__v": 0,
    "loggedInDate": "2020-09-16",
    "loggedInUserIp": null,
    "id": "5f6221e8ecfbeb4554643d88"
  },{
    "username": "Sample17",
    "firstName": "Sample Name3",
    "lastName": "Sample Last Name3",
    "roleName": "AUDITOR",
    "loggedInDate": "2020-09-16",
    "loggedInUserIp":"localhost"
    
  },{
    "_id": "5f6221e8ecfbeb4554643d88",
    "username": "Sample071",
    "firstName": "Sample Name",
    "lastName": "Sample Last Name",
    "roleId": 2,
    "createdDate": "2020-09-16T14:32:08.680Z",
    "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
    "__v": 0,
    "loggedInDate": "2020-09-16",
    "loggedInUserIp": null,
    "id": "5f6221e8ecfbeb4554643d88"
  },{
    "username": "Sample18",
    "firstName": "Sample Name3",
    "lastName": "Sample Last Name3",
    "roleName": "AUDITOR",
    "loggedInDate": "2020-09-16",
    "loggedInUserIp":"localhost"
    
  },{
    "_id": "5f6221e8ecfbeb4554643d88",
    "username": "Sample12",
    "firstName": "Sample Name",
    "lastName": "Sample Last Name",
    "roleId": 2,
    "createdDate": "2020-09-16T14:32:08.680Z",
    "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
    "__v": 0,
    "loggedInDate": "2020-09-16",
    "loggedInUserIp": null,
    "id": "5f6221e8ecfbeb4554643d88"
  },{
    "username": "Sample19",
    "firstName": "Sample Name3",
    "lastName": "Sample Last Name3",
    "roleName": "AUDITOR",
    "loggedInDate": "2020-09-16",
    "loggedInUserIp":"localhost"
    
  },{
    "_id": "5f6221e8ecfbeb4554643d88",
    "username": "Sample0711",
    "firstName": "Sample Name",
    "lastName": "Sample Last Name",
    "roleId": 2,
    "createdDate": "2020-09-16T14:32:08.680Z",
    "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
    "__v": 0,
    "loggedInDate": "2020-09-16",
    "loggedInUserIp": null,
    "id": "5f6221e8ecfbeb4554643d88"
  },{
    "username": "Sample20",
    "firstName": "Sample Name3",
    "lastName": "Sample Last Name3",
    "roleName": "AUDITOR",
    "loggedInDate": "2020-09-16",
    "loggedInUserIp":"localhost"
    
  },{
        "_id": "5f6221e8ecfbeb4554643d88",
        "username": "Sample0811",
        "firstName": "Sample Name",
        "lastName": "Sample Last Name",
        "roleId": 2,
        "createdDate": "2020-09-16T14:32:08.680Z",
        "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
        "__v": 0,
        "loggedInDate": "2020-09-16",
        "loggedInUserIp": null,
        "id": "5f6221e8ecfbeb4554643d88"
  },{
        "username": "Sample0911",
        "firstName": "Sample Name3",
        "lastName": "Sample Last Name3",
        "roleName": "AUDITOR",
        "loggedInDate": "2020-09-16",
        "loggedInUserIp":"localhost"
        
  },{
            "_id": "5f6221e8ecfbeb4554643d88",
            "username": "Sample1",
            "firstName": "Sample Name",
            "lastName": "Sample Last Name",
            "roleId": 2,
            "createdDate": "2020-09-16T14:32:08.680Z",
            "hash": "$2a$10$CKuU7zjjh/80VpOaSJGr0OPPOZpl2AOMZ15wzc6vafFhc6Tq6pxf6",
            "__v": 0,
            "loggedInDate": "2020-09-16",
            "loggedInUserIp": null,
            "id": "5f6221e8ecfbeb4554643d88"
  },{
            "username": "Sample090",
            "firstName": "Sample Name3",
            "lastName": "Sample Last Name3",
            "roleName": "AUDITOR",
            "loggedInDate": "2020-09-16",
            "loggedInUserIp":"localhost"
            
}]

export const userService = {
    login,
    logout,
    register,
    getAll,
    getById,
    update,
    getAudits,
    delete: _delete
};

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };
    return fetch(`${config.apiUrl}/users/authenticate`, requestOptions)
        .then(handleResponse)
        .then(user => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('user', JSON.stringify(user));

            return user;
        });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/users`, requestOptions).then(handleResponse);
}

function getAudits() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/audits`, requestOptions).then(response =>handleResponse(response,true));
}

function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${config.apiUrl}/users/register`, requestOptions).then(handleResponse);
}

function update(user) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${config.apiUrl}/users/${user.id}`, requestOptions).then(handleResponse);;
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}


function handleResponse(response,audits=false) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return audits? auditData : data
    });
}