import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { userActions } from '../_actions';

const columns = [{
  dataField: 'username',
  text: 'User Name'
}, {
  dataField: 'roleName',
  text: 'Role Name'
}, {
  dataField: 'loggedInDate',
  text: 'loggedIn Date'
},{
  dataField: 'loggedInUserIp',
  text: 'User Ip Address'
}];


const styles = {
  '.react-bootstrap-table-pagination-list' :{
    'text-align': 'center'
    // 'margin':'0px'
  }
}


class AuditPage extends React.Component{
    constructor(props){
        super(props)
        this.props.getAudits();
    }
    render(){
        const auditData = this.props.items||[];
        return(
            <React.Fragment>
                {auditData.length ?
                    <React.Fragment>
                    <h1>Audit Table</h1>
                    <BootstrapTable keyField='username' data={ auditData } columns={ columns } 
                    pagination={ paginationFactory() }  />
                    </React.Fragment> : <p><Link to='/'>BACK TO LOGIN</Link></p>
                }
            </React.Fragment>
        )
    }
}

function mapState(state) {
    const { authentication,users } = state;
    const { user } = authentication;
    const {items} = users
    return { user,items };
}

const actionCreators = {
    getAudits: userActions.getAudits
}



const connectedAuditPage = connect(mapState, actionCreators)(AuditPage);
export { connectedAuditPage as AuditPage };